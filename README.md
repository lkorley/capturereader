# Capture Reader

Analyses baccarat run to extract the volume and isotope involved in neutron capture.
Records the total capture gamma energy deposited in OD along with the proton recoil deposit energy.
All parameters stored in tree "captures".

## **Run using the `macro.py` script**

    `python macro.py Analyse/recoilFinder.C <list input files> --outfile <name of output file>`

### **Note:** The `configure.py` script can be used to create a new root analysis class skeleton based on an input tree and root file.
namespace std {}
using namespace std;
#include "rqlibProjectHeaders.h"

#include "rqlibLinkDef.h"

#include "rqlibProjectDict.cxx"

struct DeleteObjectFunctor {
   template <typename T>
   void operator()(const T *ptr) const {
      delete ptr;
   }
   template <typename T, typename Q>
   void operator()(const std::pair<T,Q> &) const {
      // Do nothing
   }
   template <typename T, typename Q>
   void operator()(const std::pair<T,Q*> &ptr) const {
      delete ptr.second;
   }
   template <typename T, typename Q>
   void operator()(const std::pair<T*,Q> &ptr) const {
      delete ptr.first;
   }
   template <typename T, typename Q>
   void operator()(const std::pair<T*,Q*> &ptr) const {
      delete ptr.first;
      delete ptr.second;
   }
};

#ifndef BaccRootConverterEvent_cxx
#define BaccRootConverterEvent_cxx
BaccRootConverterEvent::BaccRootConverterEvent() {
}
BaccRootConverterEvent::BaccRootConverterEvent(const BaccRootConverterEvent & rhs)
   : TObject(const_cast<BaccRootConverterEvent &>( rhs ))
   , iEventNumber(const_cast<BaccRootConverterEvent &>( rhs ).iEventNumber)
   , iRunNumber(const_cast<BaccRootConverterEvent &>( rhs ).iRunNumber)
   , volumes(const_cast<BaccRootConverterEvent &>( rhs ).volumes)
   , primaryParticles(const_cast<BaccRootConverterEvent &>( rhs ).primaryParticles)
   , tracks(const_cast<BaccRootConverterEvent &>( rhs ).tracks)
{
   // This is NOT a copy constructor. This is actually a move constructor (for stl container's sake).
   // Use at your own risk!
   (void)rhs; // avoid warning about unused parameter
   BaccRootConverterEvent &modrhs = const_cast<BaccRootConverterEvent &>( rhs );
   modrhs.volumes.clear();
   modrhs.primaryParticles.clear();
   modrhs.tracks.clear();
}
BaccRootConverterEvent::~BaccRootConverterEvent() {
}
#endif // BaccRootConverterEvent_cxx

#ifndef volumeInfo_cxx
#define volumeInfo_cxx
volumeInfo::volumeInfo() {
}
volumeInfo::volumeInfo(const volumeInfo & rhs)
   : sName(const_cast<volumeInfo &>( rhs ).sName)
   , iVolumeID(const_cast<volumeInfo &>( rhs ).iVolumeID)
   , dTotalEnergyDep_keV(const_cast<volumeInfo &>( rhs ).dTotalEnergyDep_keV)
   , iTotalOptPhotNumber(const_cast<volumeInfo &>( rhs ).iTotalOptPhotNumber)
   , iTotalThermElecNumber(const_cast<volumeInfo &>( rhs ).iTotalThermElecNumber)
{
   // This is NOT a copy constructor. This is actually a move constructor (for stl container's sake).
   // Use at your own risk!
   (void)rhs; // avoid warning about unused parameter
   volumeInfo &modrhs = const_cast<volumeInfo &>( rhs );
   modrhs.sName.clear();
}
volumeInfo::~volumeInfo() {
}
#endif // volumeInfo_cxx

#ifndef primaryParticleInfo_cxx
#define primaryParticleInfo_cxx
primaryParticleInfo::primaryParticleInfo() {
}
primaryParticleInfo::primaryParticleInfo(const primaryParticleInfo & rhs)
   : sName(const_cast<primaryParticleInfo &>( rhs ).sName)
   , dEnergy_keV(const_cast<primaryParticleInfo &>( rhs ).dEnergy_keV)
   , dTime_ns(const_cast<primaryParticleInfo &>( rhs ).dTime_ns)
   , sVolumeName(const_cast<primaryParticleInfo &>( rhs ).sVolumeName)
   , iVolumeID(const_cast<primaryParticleInfo &>( rhs ).iVolumeID)
{
   // This is NOT a copy constructor. This is actually a move constructor (for stl container's sake).
   // Use at your own risk!
   (void)rhs; // avoid warning about unused parameter
   primaryParticleInfo &modrhs = const_cast<primaryParticleInfo &>( rhs );
   modrhs.sName.clear();
   for (Int_t i=0;i<3;i++) dPosition_mm[i] = rhs.dPosition_mm[i];
   for (Int_t i=0;i<3;i++) dDirection[i] = rhs.dDirection[i];
   modrhs.sVolumeName.clear();
}
primaryParticleInfo::~primaryParticleInfo() {
}
#endif // primaryParticleInfo_cxx

#ifndef trackInfo_cxx
#define trackInfo_cxx
trackInfo::trackInfo() {
}
trackInfo::trackInfo(const trackInfo & rhs)
   : sParticleName(const_cast<trackInfo &>( rhs ).sParticleName)
   , iParticleID(const_cast<trackInfo &>( rhs ).iParticleID)
   , iTrackID(const_cast<trackInfo &>( rhs ).iTrackID)
   , iParentID(const_cast<trackInfo &>( rhs ).iParentID)
   , sCreatorProcess(const_cast<trackInfo &>( rhs ).sCreatorProcess)
   , steps(const_cast<trackInfo &>( rhs ).steps)
   , dWavelength_nm(const_cast<trackInfo &>( rhs ).dWavelength_nm)
   , dCharge(const_cast<trackInfo &>( rhs ).dCharge)
{
   // This is NOT a copy constructor. This is actually a move constructor (for stl container's sake).
   // Use at your own risk!
   (void)rhs; // avoid warning about unused parameter
   trackInfo &modrhs = const_cast<trackInfo &>( rhs );
   modrhs.sParticleName.clear();
   modrhs.sCreatorProcess.clear();
   modrhs.steps.clear();
}
trackInfo::~trackInfo() {
}
#endif // trackInfo_cxx

#ifndef stepInfo_cxx
#define stepInfo_cxx
stepInfo::stepInfo() {
}
stepInfo::stepInfo(const stepInfo & rhs)
   : iStepNumber(const_cast<stepInfo &>( rhs ).iStepNumber)
   , sProcess(const_cast<stepInfo &>( rhs ).sProcess)
   , sVolumeName(const_cast<stepInfo &>( rhs ).sVolumeName)
   , iVolumeID(const_cast<stepInfo &>( rhs ).iVolumeID)
   , dTime_ns(const_cast<stepInfo &>( rhs ).dTime_ns)
   , dParticleEnergy_keV(const_cast<stepInfo &>( rhs ).dParticleEnergy_keV)
   , dEnergyDep_keV(const_cast<stepInfo &>( rhs ).dEnergyDep_keV)
{
   // This is NOT a copy constructor. This is actually a move constructor (for stl container's sake).
   // Use at your own risk!
   (void)rhs; // avoid warning about unused parameter
   stepInfo &modrhs = const_cast<stepInfo &>( rhs );
   modrhs.sProcess.clear();
   modrhs.sVolumeName.clear();
   for (Int_t i=0;i<3;i++) dPosition_mm[i] = rhs.dPosition_mm[i];
   for (Int_t i=0;i<3;i++) dDirection[i] = rhs.dDirection[i];
}
stepInfo::~stepInfo() {
}
#endif // stepInfo_cxx


//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Feb 17 12:45:13 2022 by ROOT version 6.20/04
// from TTree DataTree/Data tree
// found on file: /scratch/penningb_root/penningb1/lkorley/Sims/dd_neutrons/BACCARAT_6.2.15/dd_neutrons_6341522.root
//////////////////////////////////////////////////////////

#ifndef recoilFinder_h
#define recoilFinder_h

#include <map>
#include <array>

#include <TH1F.h>
#include <TH2F.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include "BaccRootConverterEvent.h"

typedef struct EnT_S{
      double dTe=0;
      double dTl=0;
      double dEn=0;
      int nPh=0;
      int nHits=0;
      double dThE=0;
      double dThL=0;
      double dT1=0;
      double dT2=0;
      double dT3=0;
      double dT4=0;
      //double dT4=0;
   } EnT; //Energy,first,last,nph

bool isWithin(EnT iE, double teste, double testl){ return teste>iE.dTe && testl<=iE.dTl; }

class recoilFinder : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain
   TString fOutFileName;
	Int_t fNumberOfEvents = 0;
   Int_t fNumberOfCaptures = 0;
   Int_t fNumberOfGdCap = 0;
   Int_t fNumberOfHCap = 0;
   Int_t fNumberOfOCap = 0;
   Int_t fNumberOfProt = 0;
   Int_t fNumberOfProtCap = 0;
   Int_t fNumberOfProtXe = 0;
   Int_t fNumberOfProtCapXe = 0;
   Int_t fNumberOfXe = 0;

	Int_t fPassedEvents = 0;
   int updateFreq = 5000;
   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<BaccRootConverterEvent> Event = {fReader, "Event"};

   TString otherParticles="";
   TString otherParticlesPure="";
   TString otherCaptures="";

   TTree* capTree;
   std::vector<std::string> capName;
   std::vector<std::string> capVol;
   std::vector<std::string> nCapProc;
   std::vector<std::string> pProc;
   std::vector<std::string> pDaught;
   std::vector<int> numCops;
   std::string capFirst;
   double capT;
   std::vector<bool> capCreation;
   std::vector<bool> capSameP;
   int nCapParticles;
   int nGammaDeps;
   int nProtDeps;
   int nGammas;
   std::vector<double> gammaE; // Gamma initial Energy
   std::vector<double> gammaDeps; // OD only
   std::vector<double> pgammaDeps; // OD Only proton gammas
   std::vector<double> protDeps; // OD only
   std::vector<int> capID; // remnant parent ID
   std::vector<int> gamID; // gamma parent ID
   double maxTPCscatter;
   double maxTPCscatterT;
   double gammaDepTot; // OD only
   double maxProtDep; // OD only
    double protDT; // time between capture and recoil

   bool doDerived = false;

   //Histo Limits
   int EBins = 2000, ScBins = 2000, PhBins = 2000, YBins = 1000, PhWBins = 200, SkBins = 1000;
   float minE = 0., maxE = 10000.;
   float minSc = 0., maxSc = 100000;
   float minPh = 0, maxPh = 10000;
   float minY = 0., maxY = 40.;
   float minPhW = 0., maxPhW = 1000.;
   float minSk = -20, maxSk = 20;

    // Scintillation photon  and deposit clustering width (ns); based on shaping time of 60ns
   double scintTime = 60;
   double gammaSizeCut = 2500;
   double protonSizeCut = 100;

   //particleIDs
   static constexpr int isPhoton = 0;
   static constexpr int isGamma = 22;
   static constexpr int isNeutron = 2112;
   static constexpr int isProton = 2212;

   //Event level and other special Histograms to plot

   TH2F* h_pEN;
   TH2F* h_pET;
   TH2F* h_pTR;
   TH2F* h_pER;
   TH2F* h_EvpgammaH;
   TH2F* h_EvpgammaGd;
   TH2F* h_Evpgamma;
   TH2F* h_EvpgammaAny;

   TH2F* h_XeE;

   TH2F* h_pEv; //largest proton deposit in Event vs number of deposits above 500keV
   TH2F* h_pEv_Capture;
   TH2F* h_capvp;
    TH2F* h_pvdet;
   TH2F* h_pEv_HCapture; //largest proton deposit in Event vs number of deposits above 500keV given event has H capture
   TH2F* h_pEv_GdCapture; //largest proton deposit in Event vs number of deposits above 500keV given event has Gd capture
   TH2F* h_pEv_GdCapture_vis; //largest proton deposit in Event vs number of deposits above 500keV given event has Gd capture with over 2.5MeV observed

   std::map<std::string,TH1F*> m_h1D;
   std::map<std::string,TH2F*> m_h2D;



   typedef std::pair<double,std::pair<double,double>> akernel; //a kernel found by kernel density estimater. made up of bounds and sum inside bounds

   //double testT;

   recoilFinder(TTree * /*tree*/ =0) { }
   virtual ~recoilFinder() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   // Custom functions
	virtual void SetOutputName(TString fileName){ fOutFileName = fileName; }
   virtual void SetDerived() { doDerived = true; }

   //bool isWithin(EnT iE, double testT, double scTime);
   //bool isWithinPh(EnT iE, double testT, double scTime);

   TH1F* CreateSumw2Hist(string hname, string htitle, double Xbins, double Xbin_min, double Xbin_max);
   TH1F* Create1DHist(string h1name, string Xname, string Yname, double Xbins, double Xbin_min, double Xbin_max);
   TH2F* Create2DHist(string h2name, string htitle, double Xbins, double Xbin_min, double Xbin_max, double Ybins, double Ybin_min, double Ybin_max);

   void AddParticleHistos(std::string particle, std::string process);
   void FillParticleHistos(std::string particle, std::string process, EnT iDep, double weight = 1.0);

   void doClusterE(std::vector<EnT>& mEnT, stepInfo& mStp, double scTime);

   void doClusterPh(std::vector<EnT>& mEnT, trackInfo& mTr, double scTime);

   void clearTree();

   std::pair<double, double> recoilMatch(stepInfo& pStp, trackInfo& nTrk);

   std::array<double,4> getMoments(EnT mEnT);

   ClassDef(recoilFinder,0);

};

#endif

#ifdef recoilFinder_cxx
void recoilFinder::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t recoilFinder::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void recoilFinder::clearTree()
{
   capName.clear();
   capVol.clear();
   capCreation.clear();
   gammaDeps.clear();
   nCapProc.clear();
   protDeps.clear();
   pProc.clear();
   pgammaDeps.clear();
   pDaught.clear();
   gammaE.clear();
   gamID.clear();
   capID.clear();
   nProtDeps = 0;
   capFirst="";
   capT=1e300;
   nGammaDeps = 0;
   nGammas=0;
   nCapParticles = 0;
   gammaDepTot = 0;
   maxProtDep = 0;
   maxTPCscatter = 0;
   maxTPCscatterT = -999;
}

//bool recoilFinder::isWithin(EnT iE, double testT, double scTime){ return (testT-scintTime)<iE.dTe && (testT+scTime)>=iE.dTl; }
//bool recoilFinder::isWithinPh(EnT iE, double testT, double scTime){ return testT<iE.dTe && (testT+scTime)>=iE.dTl; }

TH2F* recoilFinder::Create2DHist(string h2name, string htitle, double Xbins, double Xbin_min, double Xbin_max, double Ybins, double Ybin_min, double Ybin_max)
{
	TH2F* h = new TH2F(h2name.c_str(),  htitle.c_str(), Xbins, Xbin_min, Xbin_max, Ybins, Ybin_min, Ybin_max);
	//h->SetXTitle(("#font[132]{" + Xname + "}").c_str());
	//h->SetYTitle(("#font[132]{" + Yname + "}").c_str());
	//h->GetXaxis()->SetLabelFont(132);
	//h->GetYaxis()->SetLabelFont(132);
	h->Sumw2();
	return h;
}
TH1F* recoilFinder::Create1DHist(string h1name, string Xname, string Yname, double Xbins, double Xbin_min, double Xbin_max)
{
	TH1F* h = new TH1F(h1name.c_str(), ("#font[132]{" + h1name + "").c_str(), Xbins, Xbin_min, Xbin_max);
	h->SetXTitle(("#font[132]{" + Xname + "}").c_str());
	h->SetYTitle(("#font[132]{" + Yname + "}").c_str());
	//h->GetXaxis()->SetLabelFont(132);
	//h->GetYaxis()->SetLabelFont(132);
	// h->SetMarkerColor(color);
	// h->SetLineColor(color);
	return h;
}

TH1F* recoilFinder::CreateSumw2Hist(string hname, string htitle, double Xbins, double Xbin_min, double Xbin_max)
{
	TH1F* h = new TH1F(hname.c_str(), htitle.c_str(), Xbins, Xbin_min, Xbin_max);
   //h->SetXTitle(("#font[132]{" + Xname + "}").c_str());
	//h->SetYTitle(("#font[132]{" + Yname + "}").c_str());
	//h->GetXaxis()->SetLabelFont(132);
	//h->GetYaxis()->SetLabelFont(132);
	// h->SetMarkerColor(color);
	// h->SetLineColor(color);
	h->Sumw2();
	return h;
}

void recoilFinder::AddParticleHistos(std::string particle, std::string process)
{
   //add histograms for particle
   std::string nameStub = "h_"+particle+"_"+process;
   std::string titleStub = process+" "+particle;
   m_h1D["Deposits/"+nameStub] = CreateSumw2Hist("Deposits/"+nameStub,titleStub +"; Deposit (keV)", EBins, minE, maxE);
   fOutput->Add(m_h1D["Deposits/"+nameStub]);

   m_h1D["ScintPh/"+nameStub] = CreateSumw2Hist("ScintPh/"+nameStub,titleStub +"; Photons Emitted", ScBins, minSc, maxSc);
   fOutput->Add(m_h1D["ScintPh/"+nameStub]);
   
   m_h1D["DetPh/"+nameStub] = CreateSumw2Hist("DetPh/"+nameStub,titleStub +"; Photons Detected", PhBins, minPh, maxPh);
   fOutput->Add(m_h1D["DetPh/"+nameStub]);
   
   m_h2D["EvScint/"+nameStub] = Create2DHist("EvScint/"+nameStub,titleStub +"; Deposit (keV); Photons Emitted; ", EBins, minE, maxE, ScBins, minSc, maxSc);
   fOutput->Add(m_h2D["EvScint/"+nameStub]);
   
   m_h2D["EvDet/"+nameStub] = Create2DHist("EvDet/"+nameStub,titleStub +"; Deposit (keV); Detected Photons; ", EBins, minE, maxE, PhBins, minPh, maxPh);
   fOutput->Add(m_h2D["EvDet/"+nameStub]);

   m_h2D["EvDetWidth/"+nameStub] = Create2DHist("EvDetWidth/"+nameStub,titleStub +"; Deposit (keV); Detected Photons Width (ns); ", EBins, minE, maxE, PhWBins, minPhW, maxPhW);
   fOutput->Add(m_h2D["EvDetWidth/"+nameStub]);
    
    m_h2D["EvDetSig/"+nameStub] = Create2DHist("EvDetSig/"+nameStub,titleStub +"; Deposit (keV); Detected Photons Sigma (ns); ", EBins, minE, maxE, PhWBins, minPhW, maxPhW);
   fOutput->Add(m_h2D["EvDetSig/"+nameStub]);
    
    m_h2D["EvDetKurt/"+nameStub] = Create2DHist("EvDetKurt/"+nameStub,titleStub +"; Deposit (keV); Detected Photons Kurt; ", EBins, minE, maxE, 1000, -50., 100.);
   fOutput->Add(m_h2D["EvDetKurt/"+nameStub]);

   m_h2D["PhvDetWidth/"+nameStub] = Create2DHist("PhvDetWidth/"+nameStub,titleStub +"; Detected Photons ; Detected Photons Width (ns); ", PhBins, minPh, maxPh, PhWBins, minPhW, maxPhW);
   fOutput->Add(m_h2D["PhvDetWidth/"+nameStub]);

   m_h2D["PhvSkew/"+nameStub] = Create2DHist("PhvSkew/"+nameStub,titleStub +"; Detected Photons; Detected Photons Normalized Skew; ", PhBins, minPh, maxPh, SkBins, minSk, maxSk);
   fOutput->Add(m_h2D["PhvSkew/"+nameStub]);

   m_h2D["ScintvDetWidth/"+nameStub] = Create2DHist("ScintvDetWidth/"+nameStub,titleStub +"; Emitted Photons Width (ns); Detected Photons Width (ns); ", PhWBins, minPhW, maxPhW, PhWBins, minPhW, maxPhW);
   fOutput->Add(m_h2D["ScintvDetWidth/"+nameStub]);

   m_h2D["EvYield/"+nameStub] = Create2DHist("EvYield/"+nameStub,titleStub +"; Deposit (keV); Yield (phe/keV); ", EBins, minE, maxE, YBins, minY, maxY);
   fOutput->Add(m_h2D["EvYield/"+nameStub]);

}

void recoilFinder::FillParticleHistos(std::string particle, std::string process, EnT iDep, double weight)
{
   //fill histograms for particle and process
   std::string nameStub = "h_"+particle+"_"+process;
   //std::string titleStub = process+" "+particle;
   //grab and fill histos
   //Info("FillParticleHistos","Filling %s",nameStub.c_str());
   std::array<double,4> moments = getMoments(iDep);

   m_h1D["Deposits/"+nameStub]->Fill(iDep.dEn, weight);
   m_h1D["ScintPh/"+nameStub]->Fill(iDep.nPh, weight);
   m_h1D["DetPh/"+nameStub]->Fill(iDep.nHits, weight);
   m_h2D["EvScint/"+nameStub]->Fill(iDep.dEn, iDep.nPh, weight);
   m_h2D["EvDet/"+nameStub] ->Fill(iDep.dEn, iDep.nHits, weight);
   m_h2D["EvDetWidth/"+nameStub]->Fill(iDep.dEn, iDep.dThL - iDep.dThE, weight);
   m_h2D["EvDetSig/"+nameStub]->Fill(iDep.dEn, moments[1], weight);
   m_h2D["EvDetKurt/"+nameStub]->Fill(iDep.dEn, moments[3], weight);
   m_h2D["PhvDetWidth/"+nameStub]->Fill(iDep.nHits, iDep.dThL - iDep.dThE, weight);
   m_h2D["PhvSkew/"+nameStub]->Fill(iDep.nHits, moments[2], weight);
   m_h2D["ScintvDetWidth/"+nameStub]->Fill(iDep.dTl - iDep.dTe, iDep.dThL - iDep.dThE, weight);
   m_h2D["EvYield/"+nameStub]->Fill(iDep.dEn, (double)iDep.nPh/iDep.dEn, weight);
   //Info("FillParticleHistos","Finished Filling %s",nameStub.c_str());

}

void recoilFinder::doClusterE(std::vector<EnT>& mEnT, stepInfo& mStp, double scTime)
{
   //do the deposit clustering here
   //Info("doClusterE","deposit clustering");
   double testu = mStp.dTime_ns + scTime ;
   double testl = mStp.dTime_ns - scTime ;
   auto itE = std::find_if(mEnT.begin(), mEnT.end(), [&testu,&testl](EnT iE){return isWithin(iE,testu,testl);});
   if(itE == mEnT.end()){// if is contained within previous dep then append
      EnT testDep = {.dTe = mStp.dTime_ns, .dTl = mStp.dTime_ns, .dEn = mStp.dEnergyDep_keV, .dThE = 1e100, .dThL = 0};
      mEnT.push_back(testDep);
   }
   else
   {
      int depIdx = itE - mEnT.begin();
      mEnT[depIdx].dTe = std::min(mStp.dTime_ns, mEnT[depIdx].dTe);
      mEnT[depIdx].dTl = std::max(mStp.dTime_ns, mEnT[depIdx].dTl);
      mEnT[depIdx].dEn += mStp.dEnergyDep_keV;
   }
   //Info("doClusterE","deposit clustering complete");

}

void recoilFinder::doClusterPh(std::vector<EnT>& mEnT, trackInfo& mTr, double scTime)
{
   //do the deposit clustering here
   //Info("doClusterPh","photon clustering");
   double testu = mTr.steps[0].dTime_ns ;
   double testl = mTr.steps[0].dTime_ns - scTime ;
   int depIdx = std::find_if(mEnT.begin(), mEnT.end(), [&testu,&testl](EnT iE){return isWithin(iE,testu,testl);}) - mEnT.begin();
   //Info("doClusterPh","cluster %d of %d",depIdx,mEnT.size());
   if(depIdx<mEnT.size())
   {
      mEnT[depIdx].dTl = std::max(mTr.steps[0].dTime_ns, mEnT[depIdx].dTl);
      mEnT[depIdx].nPh += 1;
      if(mTr.steps.back().sVolumeName.find("Water_PMT_Photocathode") != std::string::npos){
         mEnT[depIdx].nHits += 1;
         //hit times here
         mEnT[depIdx].dThE = std::min(mTr.steps.back().dTime_ns, mEnT[depIdx].dThE);
         mEnT[depIdx].dThL = std::max(mTr.steps.back().dTime_ns, mEnT[depIdx].dThL);
         mEnT[depIdx].dT1 += mTr.steps.back().dTime_ns;
         mEnT[depIdx].dT2 += mTr.steps.back().dTime_ns*mTr.steps.back().dTime_ns;
         mEnT[depIdx].dT3 += mTr.steps.back().dTime_ns*mTr.steps.back().dTime_ns*mTr.steps.back().dTime_ns;
         mEnT[depIdx].dT4 += mTr.steps.back().dTime_ns*mTr.steps.back().dTime_ns*mTr.steps.back().dTime_ns*mTr.steps.back().dTime_ns;
      }
   } //Unknown what to do if this part fails
   //Info("doClusterPh","photon clustering complete");

}

std::pair<double, double> recoilFinder::recoilMatch(stepInfo& pStp, trackInfo& nTrk)
{
   double nEn = 0;
   double nT = 0;
   std::string nProc = "";
   for(auto nStp : nTrk.steps)
   {
      if(nStp.dTime_ns<=pStp.dTime_ns && nStp.dTime_ns>nT && pStp.dParticleEnergy_keV<=nStp.dParticleEnergy_keV && (nStp.sProcess.compare("hadElastic")==0 || nStp.sProcess.compare("neutronInelastic")==0)){
         nEn = nStp.dParticleEnergy_keV;
         nT = nStp.dTime_ns;
         nProc = nStp.sProcess;
      }
   }
   //if(nEn>0)
   //Info("recoilMatch","Matched to: %s %f ns, %f keV with ratio of %f",nProc.c_str(),pStp.dTime_ns-nT,nEn,pStp.dParticleEnergy_keV/nEn);
   return std::make_pair(pStp.dTime_ns-nT,nEn);
}

std::array<double,4> recoilFinder::getMoments(EnT mEnT)
{
   double mean = mEnT.dT1/(double)mEnT.nHits;
   double sig = sqrt((mEnT.dT2/(double)mEnT.nHits) - (mean*mean));
   double skew = (mEnT.dT3/(double)mEnT.nHits) - (3*mean*sig*sig) - (mean*mean*mean);
   double kurt = (mEnT.dT4/(double)mEnT.nHits) - (4*skew*mean) - (6*mean*mean*sig*sig) - (mean*mean*mean*mean);
   std::array<double,4> result = {mean,sig,skew/(sig*sig*sig),(kurt/(sig*sig*sig*sig))-3.};
   return result;
}


#endif // #ifdef recoilFinder_cxx
